//
//  WebViewController.swift
//  Autocomplete
//
//  Created by Timothy P Miller on 2/20/15.
//  Copyright (c) 2015 Timothy P Miller. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController, WKUIDelegate {
    var urlPath: String?
    @IBOutlet var indicatorView: UIActivityIndicatorView!
    @IBOutlet var webView: WKWebView! = {
        let webConfiguration = WKWebViewConfiguration()
        let webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.translatesAutoresizingMaskIntoConstraints = false
        return webView
    }()
    
    func setupUI() {
        self.view.backgroundColor = .white
        self.view.addSubview(webView)
            
        NSLayoutConstraint.activate([
            webView.topAnchor
                    .constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            webView.leftAnchor
                    .constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
            webView.bottomAnchor
                    .constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor),
            webView.rightAnchor
                    .constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor)
        ])
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
        if let url = urlPath {
            print("URL: \(url)")
        
            if let url = URL(string: url) {
                let request = URLRequest(url: url);
                webView.load(request)
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
