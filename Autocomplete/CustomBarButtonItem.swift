//
//  CustomBarButtonItem.swift
//  Autocomplete
//
//  Created by Timothy P Miller on 12/21/20.
//  Copyright © 2020 Timothy P Miller. All rights reserved.
//

import UIKit

class CustomBarButtonItem: UIBarButtonItem {
    var searchTextField: UITextField?
}
